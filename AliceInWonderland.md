# Alice's Adventures in Wonderland  
![Alice's Adventures in Wonderland](https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png)
## ALICE'S ADVENTURES IN WONDERLAND  

### BY LEWIS CARROLL
*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998  

[A BookVirtual Digital Edition, v1.2](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf&ved=2ahUKEwjh8qGzo5uAAxX_dKQEHQH0CwYQFnoECBUQAQ&usg=AOvVaw1g3f6kLbJNHl5RuqBttq4a)  
November, 2000
![Alice's Adventures in Wonderland](https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg)
<pre>
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.



Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?



Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not <em>more</em> than once a minute.



Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.



And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It <em>is</em> next time!"
  The happy voices cry.



Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.



Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
</pre>

# Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
--- 
1

![DOWN THE RABBIT-HOLE](https://www.gutenberg.org/files/19778/19778-h/images/p001.png)
## Chapter I
### DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, “ and what is  

&nbsp;&nbsp;&nbsp;&nbsp;Down, down, down. Would the fall *never*  
come to an end ? “ I wonder how many miles  
I ’ve fallen by this time ?” she said aloud. “I  
must be getting somewhere near the centre of  
the earth. Let me see : that would be four  
thousand miles down, I think—” (for, you see,  
Alice had learnt several things of this sort in  
her lessons in the schoolroom, and though this  
was not a *very* good opportunity for showing off  
her knowledge, as there was no one to listen to  

---
25

of a mouse—to a mouse—a mouse—O mouse!”)  
The Mouse looked at her rather inquisitively,  
and seemed to her to wink with one of its  
little eyes, but it said nothing.  
“Perhaps it doesn’t understand English,”  
thought Alice; “I daresay it ’s a French mouse,  
come over with William the Conqueror.” (For,  
with all her knowledge of history, Alice had no  
very clear notion how long ago anything had  
happened.) So she began again : “Ou est ma  
chatte ?” which was the first sentence in her  
French lesson-book. The Mouse gave a sudden  
leap out of the water, and seemed to quiver  
all over with fright. “Oh, I beg your pardon !”  
cried Alice hastily, afraid that she had hurt the  
poor animal’s feelings. “ I quite forgot you didn’t  
like cats.”  
“Not like cats!” cried the Mouse, in a shrill,  
passionate voice. “Would *you* like cats if you  
were me?”  
“Well, perhaps not,” said Alice in a sooth-  
ing tone : “don’t be angry about it. And yet

---
34

forehead, (the position in which you usually  
see Shakespeare, in the pictures of him,) while  
the rest waited in silence. At last the Dodo said,  
“*Everybody* has won, and all must have prizes.”  
&nbsp;&nbsp;&nbsp;&nbsp;“ But who is to give the prizes ?” quite a  
chorus of voices asked.  
&nbsp;&nbsp;&nbsp;&nbsp;“Why, *she*, of course,” said the Dodo, point-  
ing to Alice with one finger; and the whole  
party at once crowded round her, calling out in  
a confused way, “Prizes! Prizes !”  

---
35

![A CAUCUS-RACE AND A LONG TALE](https://www.gutenberg.org/files/19778/19778-h/images/p033.png)

Then they all crowded round her once more,  
while the Dodo solemnly presented the thimble,  
saying, "We beg your acceptance of this elegant  
thimble;" and, when it had finished this short  
speech, they all cheered.  

---
39

Alice replied eagerly, for she was always  
ready to talk about her pet. “ Dinah ’s our  
cat. And she’s such a capital one for catching  
mice you can ’t think ! And oh, I wish you  
could see her after the birds! Why, she ’ll eat  
a little bird as soon as look at it !”  
This speech caused a remarkable sensation  
among the party. Some of the birds hurried  
off at once : one old magpie began wrapping  
itself up very carefully, remarking, “ I really  
must be getting home ; the night-air doesn ’t  

---
47

“ Mary Ann ! Mary Ann !” said the voice,  
“fetch me my gloves this moment !” Then came  
a little pattering of feet on the stairs. Alice  
knew it was the Rabbit coming to look for her,  
and she trembled till she shook the house, quite  
forgetting that she was now about a thousand  
times as large as the Rabbit, and had no reason  
to be afraid of it.  
Presently the Rabbit came up to the door,  
and tried to open it, but as the door opened  
inwards, and Alice’s elbow was pressed hard  
against it, that attempt proved a failure. Alice  
heard it say to itself, “Then I’ll go round and  
get in at the window.”  

---
83

“They all can,” said the Duchess ; “and  
most of ’em do.”  
“I don’t know of any that do,” Alice said  
very politely, feeling quite pleased to have got  
into a conversation.  
“You don’t know much,” said the Duchess ;  
“and that ’s a fact.”  

---
94

&nbsp;&nbsp;&nbsp;&nbsp;“Well! I've often seen a cat without a grin,”  
thought Alice ; “ but a grin without a cat !  
It’ s the most curious thing I ever saw in all  
my life !”  
&nbsp;&nbsp;&nbsp;&nbsp;She had not gone much farther before she  
came in sight of the house of the March Hare:  
she thought it must be the right house, because  
the chimneys were shaped like ears and the roof  
was thatched with fur. It was so large a house,  
that she did not like to go nearer till she had  
nibbled some more of the left-hand bit of mush-  
room, and raised herself to about two feet high :  
even then she walked up towards it rather  
timidly, saying to herself, “ Suppose it should  
be raving mad after all! I almost wish I’d gone  
to see the Hatter instead !”  

---

&nbsp;&nbsp;&nbsp;&nbsp;“Yes, but some crumbs must have got in  
as well, ”  the Hatter grumbled: “ you shouldn’t  
have put it in with the bread-knife.”  
&nbsp;&nbsp;&nbsp;&nbsp;The March Hare took the watch and looked  
at it gloomily : then he dipped it into his cup  
of tea, and looked at it again : but he could  
think of nothing better to say than his first  
remark, “ It was the _best_ butter, you know.”  
&nbsp;&nbsp;&nbsp;&nbsp;Alice had been looking over his shoulder with  
some curiosity.  “ What a funny watch !” she  
remarked. “It tells the day of the month, and  

---
107

&nbsp;&nbsp;&nbsp;&nbsp;Alice did not quite know what to say to  
this: so she helped herself to some tea an  
bread-and-butter, and then turned to the Dor-  
mouse, and repeated her question. “Why did  
they live at the bottom of a well ?”  
&nbsp;&nbsp;&nbsp;&nbsp;The Dormouse again took a minute or two  
to think about it, and then said, “ It was a  
treacle-well.”  

---
108

&nbsp;&nbsp;&nbsp;&nbsp;“Treacle,” said the Dormouse, without consider-  
ing at all this time.  
&nbsp;&nbsp;&nbsp;&nbsp;“I want a clean cup,” interrupted the Hatter:  
“let ’s all move one place on.”  
&nbsp;&nbsp;&nbsp;&nbsp;He moved on as he spoke, and the Dormouse  
followed him: the March Hare moved into the  
Dormouse’s place, and Alice rather unwillingly  
took the place of the March Hare. The Hatter  
was the only one who got any advantage from  
the change : and Alice was a good deal worse  
off than before, as the March Hare had just up-  
set the milk-jug into his plate.

---
119

“Their heads are gone, if it please your  
Majesty!” the soldiers shouted in reply.  
“That’s right!” shouted the Queen. “Can  
you play croquet?”  
The soldiers were silent, and looked at Alice,  
as the question was evidently meant for her.  
“Yes!” shouted Alice.  
“Come on then!” roared the Queen, and  
Alice joined the procession, wondering very  
much what would happen next.  
“ It’s—it’s a very fine day!” said a timid  
voice at her side. She was walking by the White  

---
136

&nbsp;&nbsp;&nbsp;&nbsp;“ A fine day, your Majesty !” the Duchess   
began in a low, weak voice.  
&nbsp;&nbsp;&nbsp;&nbsp;“Now, I give you fair warning,” shouted the  
Queen, stamping on the ground as she spoke;  
“either you or your head must be off, and that  
in about half no time ! Take your choice !”  
&nbsp;&nbsp;&nbsp;&nbsp;The Duchess took her choice, and was gone  
in a moment.  
&nbsp;&nbsp;&nbsp;&nbsp;“Let ’s go on with the game,” the Queen  
said to Alice, and Alice was too much frightened  
to say a word, but slowly followed her back to  
the croquet-ground.  

***
154  

*"It does the boots and shoes,"* the Gryphon  
replied very solemnly.  
Alice was througly puzzled. "Does the  
boots and shoes!" she repeated in a wonder-  
ing tone.  
"Why, what are *your* shoes done with?"  
said the Gryphon. "I mean, what makes them  
so shiny?"  
Alice looked down at them, and considered  
a little before she gave her answer. "They're  
done with blacking, I believe."  
"Boots and shoes under the sea," the Gry-  
phon went on in a deep voice, "are done with  
whiting. Now you know."  
"And what are they made of?" Alice asked  
in a tone of great curiosity.  
"Soles and eels, of course," the Gryphon  
replied rather impatiently: "any shrimp could  
have told you that."  
"If I'd been the whiting," said Alice, whose  
thoughts were still running on the song, "I'd  
have said to the porpoise, 'Keep back, please:  

--- 
156

So Alice began telling them her adventures  
from the time when she first saw the White  
Rabbit: she was a little nervous about it just at  
first, the two creatures got so close to her, one  
on each side, and opened their eyes and mouths  
so very wide, but she gained courage as she  
went on. Her listeners were perfectly quiet  
till she got to the part about her repeating  
“*You are old, Father William,*” to the Cater-  
pillar, and the words all coming different, and  
then the Mock Turtle drew a long breath, and  
said, “That’s very curious.”  

“It’s all about as curious as it can be,” said  
the Gryphon.  

“It all came different!” the Mock Turtle  
repeated thoughtfully. “I should like to hear  
her try and repeat something now. Tell her  
to begin.” He looked at the Gryphon as if he  
thought it had some kind of authority over  
Alice.  

"Stand up and repeat '*Tis the voice of the  
sluggard,*’” said the Gryphon.  

---
173
	
on the floor, as it is.”  
“Then you may *sit* down,” the King replied.  
Here the other guinea-pig cheered, and was  
suppressed.  
![](https://www.gutenberg.org/files/19778/19778-h/images/p160.png)  
“Come, that finished the guinea-pigs!” thought  
Alice. “Now we shall get on better.”  
“I’d rather finish my tea,” said the Hatter,  
with an anxious look at the Queen, who was  
reading the list of singers.  
“ You may go,” said the King, and the  
Hatter hurriedly left the court, without even  
waiting to put his shoes on.  

--- 
181

ALICE’S EVIDENCE

“What’s in it ?” said the Queen.  
“ I haven’t opened it yet,” said the White  
Rabbit, “ but it seems to be a letter, written by  
the prisoner to—to somebody.”  
“ It must have been that,” said the King,  
“ unless it was written to nobody, which isn’t  
usual, you know.”  
“ Who is it directed to ?” said one of the  
jurymen.  
“ It isn’t directed at all,” said the White  
Rabbit; “ in fact, there ’s nothing written on the  
*outside.*” He unfolded the paper as he spoke,  
and added, “ It isn ’t a letter, after all : it ’s a  
set of verses.”  
“ Are they in the prisoner’s handwriting ?”  
asked another of the jurymen.  
“ No, they ’re not,” said the White Rabbit,  
“and that ’s the queerest thing about it.” (The  
jury all looked puzzled.)  
“He must have imitated somebody else’s  
hand,” said the King. (The jury all brightened  
up again.)  

---
188

![](https://www.gutenberg.org/files/19778/19778-h/images/p174.png)  
At this the whole pack rose up into the air,  
and came flying down upon her; she gave a  

